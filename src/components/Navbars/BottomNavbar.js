import React from "react";
// nodejs library that concatenates strings
import { Container } from "reactstrap";

function BottomNavbar() {
    return (
        <div className="footer-container fixed-bottom"
            style={{ height: "3rem", backgroundColor: "#fc8a23", padding: "10px", paddingLeft: "5%", paddingRight: "5%" }}>
                <div className="footer-size" style={{ float: "left", color: "white", fontWeight: "300", fontStyle: "italic" }}>
                    Tampilan terbaik di Google Chrome dan Mozilla Firefox dengan resolusi 1280 x 800
                </div>
                <div className="footer-size" style={{ float: "right", color: "white", textAlign: "right", fontWeight: "300"}}>
                    Copyright © 2019 PT.Bank Rakyat Indonesia (Persero) Tbk<br />All Rights Reserved
                </div>
        </div>
    );
}

export default BottomNavbar;
