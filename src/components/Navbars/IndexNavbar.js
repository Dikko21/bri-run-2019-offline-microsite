import React from "react";
// nodejs library that concatenates strings
import classnames from "classnames";
// reactstrap components
import {
  Button,
  Collapse,
  NavbarBrand,
  Navbar,
  NavItem,
  NavLink,
  Nav,
  Container
} from "reactstrap";
import { Link, animateScroll as scroll } from "react-scroll";

import Logo1 from '../../assets/img/logo-124.png';
import Logo2 from '../../assets/img/Logo_BR_biru.png';

function IndexNavbar() {
  const [navbarColor, setNavbarColor] = React.useState("navbar-transparent");
  const [navbarCollapse, setNavbarCollapse] = React.useState(false);

  const toggleNavbarCollapse = () => {
    setNavbarCollapse(!navbarCollapse);
    document.documentElement.classList.toggle("nav-open");
  };

  const scrollToTop = () => {
    scroll.scrollToTop();
  };


  React.useEffect(() => {
    const updateNavbarColor = () => {
      if (
        document.documentElement.scrollTop > 299 ||
        document.body.scrollTop > 299
      ) {
        setNavbarColor("");
      } else if (
        document.documentElement.scrollTop < 300 ||
        document.body.scrollTop < 300
      ) {
        setNavbarColor("");
      }
    };

    window.addEventListener("scroll", updateNavbarColor);

    return function cleanup() {
      window.removeEventListener("scroll", updateNavbarColor);
    };
  });
  return (
    <Navbar className={classnames("fixed-top")} expand="lg" style={{ height: "100px", paddingLeft: "100px", paddingRight: "200px" }}>
      <img className="logo-124" src={Logo1} alt="Logo" />
      <img className="logo-br" src={Logo2} alt="Logo" />
      <div className="navbar-translate" style={{ maxWidth: "99vw" }}>
        <NavbarBrand>
          {/* Beranda */}
        </NavbarBrand>
        <button
          aria-expanded={navbarCollapse}
          className={classnames("navbar-toggler navbar-toggler", {
            toggled: navbarCollapse
          })}
          onClick={toggleNavbarCollapse}
          style={{marginRight: "-170px"}}
        >
          <span className="navbar-toggler-bar bar1" />
          <span className="navbar-toggler-bar bar2" />
          <span className="navbar-toggler-bar bar3" />
        </button>
      </div>
      <Collapse
        className="justify-content-end"
        navbar
        isOpen={navbarCollapse}
        style={{ backgroundColor: "white" }}
      >
        <Nav navbar>
          <NavItem>
            <NavLink>
              <Link>
                <i className="title-navbar" onClick={scrollToTop}>Beranda</i>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link activeClass="active" to="download" spy={true} smooth={true}
                offset={-100}
                duration={500}
              >
                <i className="title-navbar">Unduh</i>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link activeClass="active" to="route" spy={true} smooth={true}
                offset={-100}
                duration={500}
              >
                <i className="title-navbar">Rute</i>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link activeClass="active" to="rpc" spy={true} smooth={true}
                offset={-100}
                duration={500}
              >
                <i className="title-navbar">Race pack</i>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link activeClass="active" to="faq" spy={true} smooth={true}
                offset={-100}
                duration={500}
              >
                <i className="title-navbar">FAQ</i>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link activeClass="active" to="media" spy={true} smooth={true}
                offset={-100}
                duration={500}
              >
                <i className="title-navbar">Hubungi Kami</i>
              </Link>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink>
              <Link activeClass="active" to="loc" spy={true} smooth={true}
                offset={-100}
                duration={500}
              >
                <i className="title-navbar">Lokasi</i>
              </Link>
            </NavLink>
          </NavItem>
          {/* <NavItem>
            <NavLink href="/gallery">
                <i className="title-navbar">Gallery</i>
            </NavLink>
          </NavItem> */}
        </Nav>
      </Collapse>
    </Navbar>
  );
}

export default IndexNavbar;
