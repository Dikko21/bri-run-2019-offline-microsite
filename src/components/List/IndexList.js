import React, { useState } from "react";
import { Redirect } from 'react-router-dom'

// reactstrap components
import { Container } from "reactstrap";
import BackgroundList from "./BackgroundList.js";

// core components

function IndexList() {
  return (
    <>
      <div>
        <BackgroundList />
      </div>
    </>
  );
}

export default IndexList;