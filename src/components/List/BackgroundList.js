import React, { useState, useEffect } from 'react';
import {
    Container,
    Row,
    Col,
    Pagination,
    PaginationItem,
    PaginationLink,
} from "reactstrap";

import List from "./List.js";

import background from '../../assets/img/BG_plain_original.png';

const BackgroundApp = () => {
    return (
        <div
            style={{
                backgroundImage: `url(${background})`,
                backgroundPosition: "left",
                backgroundSize: "cover",
                minHeight: "0",
                height: "100%"
            }}>
            <div style={{
                position: "fixed",
                top: 0,
                backgroundColor: "white",
                width: "100%",
                height: "80px",
                zIndex: "1"
            }}>
            </div>
            <div style={{
                position: "fixed",
                top: "20px",
                left: "50%",
                zIndex: "1"
            }}>
                <div style={{ marginLeft: "-50%" }}>
                    <Pagination className="mr-auto ml-auto">
                        <PaginationItem>
                            <PaginationLink
                                aria-label="Previous"
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                <i aria-hidden={true} className="fa fa-angle-left" />
                                <span className="sr-only">Previous</span>
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                1
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                2
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                3
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                4
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                5
                            </PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink
                                aria-label="Next"
                                href="#pablo"
                                onClick={e => e.preventDefault()}
                            >
                                <i aria-hidden={true} className="fa fa-angle-right" />
                                <span className="sr-only">Next</span>
                            </PaginationLink>
                        </PaginationItem>
                    </Pagination>
                </div>
            </div>
            <div className="btn"
                style={{
                    position: "fixed",
                    top: "20px",
                    left: "20px",
                    borderRadius: "25em",
                    zIndex: "1"
                }}>
                <i class="nc-icon nc-minimal-left" style={{ top: "0" }} /> Back
            </div>
            <div style={{ paddingTop: "80px" }}>
                <br />
                <Container style={{ backgroundColor: "white", opacity: "0.7", borderRadius: "10px" }}>
                    <br />
                    <List />
                </Container>
            </div>
            <br />
        </div>
    );
};

export default BackgroundApp;