import React, { Component } from 'react';
import { Modal, Table, Button, Row, Col } from 'reactstrap';
import TableList from './TableList.js';

class List extends Component {
    render() {
        const dummy = [{ "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" },
        { "nama": "dummy", "email": "dummy@mail" }]
        const tabel = dummy.map((data, index) => <TableList key={index} index={index + 1} nama={data.nama} email={data.email} />)
        return (
            <div>
                <Row className="mr-auto ml-auto">
                    <Col xs="2" style={{ textAlign: "center", fontWeight: "bold", color: "black" }}>
                        No.
                    </Col>
                    <Col xs="5" style={{ textAlign: "center", fontWeight: "bold", color: "black"  }}>
                        Nama
                    </Col>
                    <Col xs="5" style={{ textAlign: "center", fontWeight: "bold", color: "black"  }}>
                        Email
                    </Col>
                </Row>
                <hr />
                {tabel}
            </div>
        );
    }
}

export default List;