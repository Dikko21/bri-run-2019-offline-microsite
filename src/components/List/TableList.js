import React from 'react';
import { Table, Row, Col } from 'reactstrap';

const TableList = (props) => {
    return (
        <div>
            <Row className="mr-auto ml-auto">
                <Col xs="2" style={{ textAlign: "center", color: "black", fontWeight: "500" }}>
                    {props.index}
                </Col>
                <Col xs="5" style={{ textAlign: "center", color: "black", fontWeight: "500" }}>
                    {props.nama}
                </Col>
                <Col xs="5" style={{ textAlign: "center", color: "black", fontWeight: "500" }}>
                    {props.email}
                </Col>
            </Row>
            <hr />
        </div>
        // <div>
        //     <Table responsive>
        //         <tbody>
        //             <tr>
        //                 <td style={{ width: "10%", textAlign: 'center' }}>
        //                     {props.index}
        //                 </td>
        //                 <td style={{ width: "75%", textAlign: 'center' }}>
        //                     {props.nama}
        //                 </td>
        //                 <td style={{ width: "15%", textAlign: 'center' }}>
        //                     {props.email}
        //                 </td>
        //             </tr>
        //         </tbody>
        //     </Table>
        // </div>
    );
};

export default TableList;