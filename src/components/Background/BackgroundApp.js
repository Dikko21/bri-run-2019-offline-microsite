import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from "reactstrap";
import { Link, animateScroll as scroll } from "react-scroll";
import { withRouter, Route } from 'react-router-dom'
// import { useHistory } from "react-router-dom";
import Countdown from '../../views/Index-BRI/Countdown.js';

import desktopImage3 from '../../assets/img/BG_plain_original.png';
import desktopImage2 from '../../assets/img/1366x768-01.png';
import desktopImage from '../../assets/img/1280x800-01.png';
import mobileImage from '../../assets/img/1024x768-01.png';
import mobileImage2 from '../../assets/img/768x1024-01.png';
import mobileImage3 from '../../assets/img/512x1024-01.png';
import logobumn from '../../assets/img/Logo_BUMN.png';
import logo124 from '../../assets/img/Logo_124.png';
import logobankbri from '../../assets/img/Logo_BANK_BRI.png';
import logobrilianrun from '../../assets/img/Logo_BRIlian_RUN.png';

const BackgroundApp = () => {
    const imageUrl_2 = useWindowWidth() <= 0 ? desktopImage2 : desktopImage3;
    const imageUrl_1 = useWindowWidth() <= 0 ? desktopImage : imageUrl_2;
    const imageUrl = useWindowWidth() <= 0 ? mobileImage : imageUrl_1;
    const imageUrl2 = useWindowWidth() <= 0 ? mobileImage2 : imageUrl;
    const imageUrl3 = useWindowWidth() <= 0 ? mobileImage3 : imageUrl2;

    // let history = useHistory();

    function handleClick() {
        // history.push("/register");
        this.props.history.push('/register')
    }

    return (
        <div className="page-header"
            style={{
                backgroundImage: `url(${imageUrl3})`,
                marginTop: "100px",
                width: "auto",
                minHeight: "0",
                height: `calc((100vh - 100px) - 3rem)`
            }}>
            <div style={{ height: "9vw", maxHeight: "80px", position: "absolute", top: "10px", left: "3vw" }}>
                <img style={{ height: "70%", marginTop: "10%" }} src={logobumn} alt="Logo" />
            </div>
            <div style={{ height: "9vw", maxHeight: "80px", position: "absolute", top: "10px" }}>
                <img style={{ height: "100%" }} src={logo124} alt="Logo" />
            </div>
            <div style={{ height: "9vw", maxHeight: "80px", position: "absolute", top: "10px", right: "3vw" }}>
                <img style={{ height: "70%", marginTop: "8%" }} src={logobankbri} alt="Logo" />
            </div>
            <div style={{ width: "80vw", maxWidth: "700px", position: "absolute" }}>
                <img style={{ width: "100%" }} src={logobrilianrun} alt="Logo" />
            </div>
            <Container>
                <Row style={{ paddingTop: "50vh" }}>
                    <Col className="ml-auto mr-auto" md="12" lg="4" style={{ textAlign: "center", margin: "5px" }}>
                        <Link activeClass="active" to="informasi" spy={true} smooth={true}
                            offset={-100}
                            duration={500}
                        >
                            <div className="btn btn-home">
                                <span style={{ fontWeight: "550" }}>INFORMASI EVENT</span>
                            </div>
                        </Link>
                    </Col>
                    <Col className="ml-auto mr-auto" md="12" lg="4" style={{ textAlign: "center", margin: "5px" }}>
                        {/* <div className="body-countdown">
                            <Countdown />
                        </div> */}
                        {/* <div className="btn btn-home">
                            <span style={{ fontWeight: "550" }} >{Button()}</span>
                        </div> */}
                        {Button()}
                    </Col>
                    <Col className="ml-auto mr-auto" md="12" lg="4" style={{ textAlign: "center", margin: "5px" }}>
                        <Link activeClass="active" to="terms" spy={true} smooth={true}
                            offset={-100}
                            duration={500}
                        >
                            <div className="btn btn-home">
                                <span style={{ fontWeight: "550" }}>SYARAT & KETENTUAN</span>
                            </div>
                        </Link>
                    </Col>
                </Row>
            </Container>
        </div>
    );
};

const useWindowWidth = () => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    const handleWindowResize = () => {
        setWindowWidth(window.innerWidth);
    };

    useEffect(() => {
        window.addEventListener('resize', handleWindowResize);
        return () => window.removeEventListener('resize', handleWindowResize);
    }, []);

    return windowWidth;
};

const Button = () => (
    <Route render={({ history }) => (
        <div className="btn btn-home" onClick={() => { history.push('/register') }} >
            <span style={{ fontWeight: "550" }} ><span style={{ fontWeight: "550" }} >REGISTRASI</span></span>
        </div>
    )} />
)

export default BackgroundApp;