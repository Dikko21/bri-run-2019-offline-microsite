import React, { useState } from "react";
import { Row, Col, Modal, ModalBody } from "reactstrap";

const GalleryComponent = (props) => {
    const [modal1, setModal1] = useState(false);
    const toggle1 = () => setModal1(!modal1);
    return (
        <Col className="ml-auto mr-auto" xs="4" md="3" lg="2" style={{padding: "0"}}>
            <div style={{ border: "3px solid", margin: "1%", backgroundColor: "white" }}>
                <img style={{ width: "100%" }} src={props.img} alt="Logo" onClick={toggle1} />
            </div>
            <Modal isOpen={modal1} toggle={toggle1} >
                <ModalBody style={{ width: "100%" }}>
                    <img style={{ width: "100%" }} src={props.img} alt="Logo" />
                </ModalBody>
            </Modal>
        </Col>
    );
}

export default GalleryComponent