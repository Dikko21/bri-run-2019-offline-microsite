import React, { useState } from "react";
import { Row, Col, Modal, ModalBody } from "reactstrap";

//Core Component
import GalleryComponent from "./GalleryComponent.js";

//Foto
import img1 from "../../assets/img/BR2017(1).jpg";
import img2 from "../../assets/img/BR2017(2).jpg";
import img3 from "../../assets/img/BR2017(3).jpg";
import img4 from "../../assets/img/BR2017(4).jpg";
import img5 from "../../assets/img/BR2017(5).jpg";
import img6 from "../../assets/img/BR2017(6).jpg";
import img7 from "../../assets/img/BR2017(7).jpg";
import img8 from "../../assets/img/BR2017(8).jpg";
import img9 from "../../assets/img/BR2017(9).jpg";
import img10 from "../../assets/img/BR2017(10).jpg";
import img11 from "../../assets/img/BR2017(11).jpg";
import img12 from "../../assets/img/BR2017(12).jpg";

const GalleryBR2017 = () => {

    const foto = [img1, img2, img3, img4, img5, img6, img7, img8, img9, img10, img11, img12];
    const showFoto = foto.map((image) => <GalleryComponent img={image} />)

    return (
        <div>
            <Row>
                {showFoto}
            </Row>
        </div>
    );
}

export default GalleryBR2017