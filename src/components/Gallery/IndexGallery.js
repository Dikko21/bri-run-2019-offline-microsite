import React, { useState } from "react";

// reactstrap components
import { Container, TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from "reactstrap";
import classnames from 'classnames';

// core components
import GBR2016 from "./GalleryBR2016.js";
import GBR2017 from "./GalleryBR2017.js";
import GBR2018 from "./GalleryBR2018.js";
import background from '../../assets/img/BG_plain_original.png';

function IndexHeader() {

    // const [activeTab, setActiveTab] = useState('1');

    // const toggle = tab => {
    //     if (activeTab !== tab) setActiveTab(tab);
    // }

    const [activeTab, setActiveTab] = React.useState("1");
    const toggle = tab => {
        if (activeTab !== tab) {
            setActiveTab(tab);
        }
    };

    return (
        <>
            <div
                style={{
                    backgroundImage: `url(${background})`,
                    backgroundPosition: "left",
                    backgroundSize: "cover",
                    minHeight: "100vh",
                    height: "100%"
                }}>
                <Container>
                    <div className="nav-tabs-navigation" style={{marginBottom:"0"}}>
                        <div className="nav-tabs-wrapper">
                            <Nav id="tabs" role="tablist" tabs>
                                <NavItem>
                                    <NavLink
                                        className={activeTab === "1" ? "active" : ""}
                                        onClick={() => {
                                            toggle("1");
                                        }}
                                    >
                                        <div className="tabs-gallery">BRI RUN 2016</div>
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={activeTab === "2" ? "active" : ""}
                                        onClick={() => {
                                            toggle("2");
                                        }}
                                    >
                                        <div className="tabs-gallery">BRI RUN 2017</div>
                                    </NavLink>
                                </NavItem>
                                <NavItem>
                                    <NavLink
                                        className={activeTab === "3" ? "active" : ""}
                                        onClick={() => {
                                            toggle("3");
                                        }}
                                    >
                                        <div className="tabs-gallery">BRI RUN 2018</div>
                                    </NavLink>
                                </NavItem>
                            </Nav>
                        </div>
                    </div>
                    <TabContent activeTab={activeTab} className="text-center" style={{backgroundColor:"white"}}>
                        <TabPane tabId="1">
                            <GBR2016 />
                        </TabPane>
                        <TabPane tabId="2">
                            <GBR2016 />
                        </TabPane>
                        <TabPane tabId="3">
                            <GBR2016 />
                        </TabPane>
                    </TabContent>
                </Container>
                {/* <Nav tabs style={{position:"fixed", zIndex:"1", backg}}>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: activeTab === '1' })}
                            onClick={() => { toggle('1'); }}
                        >
                            BRI RUN 2016
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: activeTab === '2' })}
                            onClick={() => { toggle('2'); }}
                        >
                            BRI RUN 2017
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: activeTab === '3' })}
                            onClick={() => { toggle('3'); }}
                        >
                            BRI RUN 2018
                        </NavLink>
                    </NavItem>
                </Nav>
                <TabContent activeTab={activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col sm="12">
                                <GBR2016 />
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tabId="2">
                        <Row>
                            <Col sm="12">
                                <GBR2017 />
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tabId="3">
                        <Row>
                            <Col sm="12">
                                <GBR2018 />
                            </Col>
                        </Row>
                    </TabPane>
                </TabContent> */}
            </div>
        </>
    );
}

export default IndexHeader;
