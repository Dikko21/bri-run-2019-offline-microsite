import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import auth0Client from '../../Auth';
import IndexRegister from './IndexRegister.js';
import Registered from './Registered.js';

import background from '../../assets/img/BG_plain_original.png';

class AuthRegister extends Component {
    constructor() {
        super();
        this.state = {
            loading: true,
            registered: false
        }
    }

    componentDidMount() {
        try {
            var profile = JSON.parse(localStorage.getItem('profile'));
            localStorage.setItem("BR2019_email", profile.email)
        }
        catch (e) {
            var profile = { email: "LordKocin" }
            localStorage.setItem("BR2019_email", profile)
        }

        fetch("https://apiserver.brirun.grit.id/Cek/User", {
            method: 'POST',
            headers: { 'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8' },
            body: JSON.stringify({
                email: profile.email
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.data.length >= 1) {
                    this.setState({
                        registered: true,
                        loading: false
                    })
                }
                else {
                    this.setState({
                        registered: false,
                        loading: false
                    })
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    render() {
        return (
            <div>{!auth0Client.isAuthenticated() ?
                auth0Client.signIn() :
                this.state.loading ?
                    <div
                        style={{
                            backgroundImage: `url(${background})`,
                            backgroundPosition: "left",
                            backgroundSize: "cover",
                            minHeight: "100vh",
                            height: "100%",
                            display: "flex",
                            alignItems: "center",
                            textAlign: "center"
                        }}>
                        <div className="lds-spinner">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div> :
                    this.state.registered ? <Registered /> : <IndexRegister />
            }
            </div>
        );
    }
}

export default withRouter(AuthRegister);