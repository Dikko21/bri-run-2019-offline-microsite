import React from "react";
import {withRouter} from 'react-router-dom';

// reactstrap components
import Step1 from "./Step1"
import Step2 from "./Step2"
import Step3 from "./Step3"
import Step4 from "./Step4"
import Step5 from "./Step5"

// core components
import background from '../../assets/img/BG_plain_original.png';

class IndexRegistrasi extends React.Component {
    constructor() {
        super();
        this.state = {
            currentStep: 5
        };

        this._next = this._next.bind(this);
        this._prev = this._prev.bind(this);
    }

    _next() {
        let currentStep = this.state.currentStep;
        // Make sure currentStep is set to something reasonable
        if (currentStep >= 4) {
            currentStep = 5;
        } else {
            currentStep = currentStep + 1;
        }

        this.setState({
            currentStep: currentStep
        });
    }

    _prev() {
        let currentStep = this.state.currentStep;
        if (currentStep <= 1) {
            currentStep = 1;
        } else {
            currentStep = currentStep - 1;
        }

        this.setState({
            currentStep: currentStep
        });
    }

    componentDidMount() {
        localStorage.removeItem('BR2019_kategori', "");
        localStorage.setItem('BR2019_voucher', "");
        localStorage.removeItem('BR2019_nik', "");
        localStorage.removeItem('BR2019_nama', "");
        localStorage.removeItem('BR2019_kelamin', "");
        localStorage.removeItem('BR2019_lahir', "");
        localStorage.removeItem('BR2019_alamat', "");
        localStorage.removeItem('BR2019_kota', "");
        localStorage.removeItem('BR2019_pos', "");
        localStorage.removeItem('BR2019_telp', "");
        localStorage.removeItem('BR2019_q1', "");
        localStorage.removeItem('BR2019_q2', "");
        localStorage.removeItem('BR2019_q3', "");
        localStorage.removeItem('BR2019_q4', "");
        localStorage.removeItem('BR2019_q5', "");
        localStorage.removeItem('BR2019_nokerabat', "");
        localStorage.removeItem('BR2019_hubungan', "");
        localStorage.removeItem('BR2019_kerabat', "");
    }

    render() {
        return (
            <div
                style={{
                    backgroundImage: `url(${background})`,
                    backgroundPosition: "left",
                    backgroundSize: "cover",
                    minHeight: "0",
                    height: "100%"
                }}>
                {this.state.currentStep == 5 ? null :
                    <div className="btn"
                        onClick={this._prev}
                        style={{
                            position: "fixed",
                            top: "20px",
                            left: "20px",
                            borderRadius: "25em",
                            zIndex: "1"
                        }}>
                        <i className="nc-icon nc-minimal-left" style={{ top: "0" }} /> BACK
                    </div>}
                {this.state.currentStep == 5 ? null :
                    <div className="btn"
                        onClick={this._next}
                        style={{
                            position: "fixed",
                            top: "20px",
                            right: "20px",
                            borderRadius: "25em",
                            zIndex: "1"
                        }}>
                        Next <i className="nc-icon nc-minimal-right" style={{ top: "0" }} />
                    </div>}
                <Step1 currentStep={this.state.currentStep} />
                <Step2 currentStep={this.state.currentStep} />
                <Step3 currentStep={this.state.currentStep} />
                <Step4 currentStep={this.state.currentStep} />
                <Step5 currentStep={this.state.currentStep} />
            </div>
        );
    }
}
export default withRouter(IndexRegistrasi);