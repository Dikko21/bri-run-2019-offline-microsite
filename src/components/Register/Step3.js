import React from 'react';
import { Row, Col, FormGroup, Input, Label, Form, FormText, Container, Modal, ModalBody } from 'reactstrap';

class Step3 extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeAlamat = this.onChangeAlamat.bind(this);
        this.onChangeKota = this.onChangeKota.bind(this);
        this.onChangePos = this.onChangePos.bind(this);
        this.onChangeTelp = this.onChangeTelp.bind(this);
        this.toggle = this.toggle.bind(this)
        this.state = {
            alamat: "",
            kota: "",
            pos: "",
            telp: ""
        }
    }

    onChangeAlamat(e) {
        this.setState({
            alamat: e.target.value,
        })
        localStorage.setItem('BR2019_alamat', e.target.value);
    }

    onChangeKota(e) {
        this.setState({
            kota: e.target.value,
        })
        localStorage.setItem('BR2019_kota', e.target.value);
    }

    onChangePos(e) {
        this.setState({
            pos: e.target.value,
        })
        localStorage.setItem('BR2019_pos', e.target.value);
    }

    onChangeTelp(e) {
        this.setState({
            telp: e.target.value.replace(/\D/, ''),
        })
        localStorage.setItem('BR2019_telp', e.target.value);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        })
    }

    render() {
        if (this.props.currentStep !== 3) {
            return null;
        }
        return (
            <div
                // className="register-border" 
                style={{
                    height: "100vh",
                    width: "100vw",
                    padding: "80px 15px 45px 15px"
                }}>
                <div style={{ backgroundColor: "white", maxWidth: "600px", height: "100%", borderRadius: "25px", margin: "auto" }}>
                    <div style={{
                        textAlign: "center",
                        fontSize: "1.3rem",
                        fontWeight: "500",
                        color: "white",
                        paddingTop: "15px",
                        paddingBottom: "5px",
                        backgroundColor: "#005498",
                        borderTopLeftRadius: "21px",
                        borderTopRightRadius: "21px"
                    }}>
                        Pendaftaran Peserta
                </div>
                    <div style={{ padding: "10px", fontSize: "0.7rem", fontWeight: "600", color: "#005498" }}>
                        <Form>
                            <FormGroup>
                                <Label>ALAMAT<span style={{ color: "red" }}>*</span></Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    type="textarea"
                                    value={this.state.alamat}
                                    placeholder="Alamat Lengkap"
                                    onChange={this.onChangeAlamat} />
                            </FormGroup>
                            <FormGroup>
                                <Label>KOTA<span style={{ color: "red" }}>*</span></Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    type="text" value={this.state.kota}
                                    placeholder="Kota"
                                    onChange={this.onChangeKota} />
                            </FormGroup>
                            <FormGroup>
                                <Label>KODE POS<span style={{ color: "red" }}>*</span></Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    type="text" value={this.state.pos}
                                    placeholder="Kode Pos"
                                    onChange={this.onChangePos} />
                            </FormGroup>
                            <FormGroup>
                                <Label>TELP<span style={{ color: "red" }}>*</span></Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    maxLength="13"
                                    type="text" value={this.state.telp}
                                    placeholder="No Telp"
                                    onChange={this.onChangeTelp} />
                            </FormGroup>
                        </Form>
                        <div style={{ color: "red", fontWeight: "400" }}>
                            *) wajib diisi
                        </div>
                        {this.state.alamat != "" && this.state.kota != "" && this.state.pos != "" && this.state.telp != ""  ? null :
                            <div className="btn" onClick={this.toggle}
                                style={{
                                    position: "fixed",
                                    top: "20px",
                                    right: "20px",
                                    borderRadius: "25em",
                                    zIndex: "1",
                                    color: "transparent",
                                    backgroundColor: "transparent",
                                    borderColor: "transparent"
                                }}>
                                Next <i className="nc-icon nc-minimal-right" style={{ top: "0" }} />
                            </div>}
                        <Modal isOpen={this.state.modal} toggle={this.toggle} >
                            <ModalBody style={{ width: "100%", textAlign: "center" }}>
                                <div>Silakan isi data dengan lengkap</div>
                            </ModalBody>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

export default Step3