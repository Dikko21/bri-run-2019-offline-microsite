import React from 'react';
import {
    FormGroup,
    Input,
    Label,
    Form,
    InputGroupAddon,
    InputGroupText,
    InputGroup,
    Modal,
    ModalBody
} from 'reactstrap';
import ReactDatetime from "react-datetime";

class Step2 extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeNIK = this.onChangeNIK.bind(this);
        this.onChangeNama = this.onChangeNama.bind(this);
        this.onChangeKelamin = this.onChangeKelamin.bind(this);
        this.onChangeLahir = this.onChangeLahir.bind(this);
        this.toggle = this.toggle.bind(this)
        this.toggle1 = this.toggle1.bind(this)
        this.state = {
            nik: "",
            nama: "",
            kelamin: "",
            lahir: ""

        }
    }

    onChangeNIK(e) {
        this.setState({
            nik: e.target.value.replace(/\D/, ''),
        })
        localStorage.setItem('BR2019_nik', e.target.value);
    }

    onChangeNama(e) {
        this.setState({
            nama: e.target.value,
        })
        localStorage.setItem('BR2019_nama', e.target.value);
    }

    onChangeKelamin(e) {
        this.setState({
            kelamin: e.target.value,
        })
        localStorage.setItem('BR2019_kelamin', e.target.value);
    }

    onChangeLahir(date) {
        this.setState({
            lahir: date._d.getFullYear() + "-" + (date._d.getUTCMonth() + 1) + "-" + date._d.getDate()
        })
        localStorage.setItem('BR2019_lahir', date._d.getFullYear() + "-" + (date._d.getUTCMonth() + 1) + "-" + date._d.getDate());
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        })
    }

    toggle1() {
        this.setState({
            modal1: !this.state.modal1
        })
    }

    render() {
        if (this.props.currentStep !== 2) {
            return null;
        }

        return (
            <div
                // className="register-border" 
                style={{
                    height: "100vh",
                    width: "100vw",
                    padding: "80px 15px 45px 15px"
                }}>
                <div style={{ backgroundColor: "white", maxWidth: "600px", height: "100%", borderRadius: "25px", margin: "auto" }}>
                    <div style={{
                        textAlign: "center",
                        fontSize: "1.3rem",
                        fontWeight: "500",
                        color: "white",
                        paddingTop: "15px",
                        paddingBottom: "5px",
                        backgroundColor: "#005498",
                        borderTopLeftRadius: "21px",
                        borderTopRightRadius: "21px"
                    }}>
                        Pendaftaran Peserta
                </div>
                    <div style={{ padding: "10px", fontSize: "0.7rem", fontWeight: "600", color: "#005498" }}>
                        <Form>
                            <FormGroup>
                                <Label>NIK<span style={{ color: "red" }}>*</span></Label>
                                <Input value={this.state.nik} onChange={this.onChangeNIK} style={{ fontSize: "0.8rem" }} type="text" placeholder="NIK" maxLength="16" />
                            </FormGroup>
                            <FormGroup>
                                <Label>NAMA<span style={{ color: "red" }}>*</span></Label>
                                <Input value={this.state.nama} onChange={this.onChangeNama} style={{ fontSize: "0.8rem" }} type="text" placeholder="Nama" />
                            </FormGroup>
                            <FormGroup>
                                <Label>JENIS KELAMIN<span style={{ color: "red" }}>*</span></Label>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="radio" name="kelamin" value="Laki" onChange={this.onChangeKelamin} />{' '}
                                        Laki-laki
                                </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="radio" name="kelamin" value="Perempuan" onChange={this.onChangeKelamin} />{' '}
                                        Perempuan
                                </Label>
                                </FormGroup>
                            </FormGroup>
                            <FormGroup>
                                <Label>TGL LAHIR<span style={{ color: "red" }}>*</span></Label>
                                <InputGroup className="date" id="datetimepicker" style={{ fontSize: "0.8rem" }}>
                                    <ReactDatetime
                                        onChange={this.onChangeLahir}
                                        dateFormat="YYYY-MM-DD"
                                        timeFormat={false}
                                        inputProps={{
                                            placeholder: "Tanggal Lahir"
                                        }}
                                    />
                                    <InputGroupAddon addonType="append">
                                        <InputGroupText>
                                            <span className="glyphicon glyphicon-calendar">
                                                <i aria-hidden={true} className="fa fa-calendar" />
                                            </span>
                                        </InputGroupText>
                                    </InputGroupAddon>
                                </InputGroup>
                            </FormGroup>
                        </Form>
                        <div style={{ color: "red", fontWeight: "400" }}>
                            *) wajib diisi
                        </div>
                        {this.state.nik.length != 16 ?
                            <div className="btn" onClick={this.toggle}
                                style={{
                                    position: "fixed",
                                    top: "20px",
                                    right: "20px",
                                    borderRadius: "25em",
                                    zIndex: "1",
                                    color: "transparent",
                                    backgroundColor: "transparent",
                                    borderColor: "transparent"
                                }}>
                                Next <i className="nc-icon nc-minimal-right" style={{ top: "0" }} />
                            </div> : null}
                        {this.state.nik.nama != "" && this.state.kelamin != "" && this.state.lahir != "" ? null :
                            <div className="btn" onClick={this.toggle1}
                                style={{
                                    position: "fixed",
                                    top: "20px",
                                    right: "20px",
                                    borderRadius: "25em",
                                    zIndex: "1",
                                    color: "transparent",
                                    backgroundColor: "transparent",
                                    borderColor: "transparent"
                                }}>
                                Next <i className="nc-icon nc-minimal-right" style={{ top: "0" }} />
                            </div>}
                        <Modal isOpen={this.state.modal} toggle={this.toggle} >
                            <ModalBody style={{ width: "100%", textAlign: "center" }}>
                                <div>Silakan isi NIK dengan benar</div>
                            </ModalBody>
                        </Modal>
                        <Modal isOpen={this.state.modal1} toggle={this.toggle1} >
                            <ModalBody style={{ width: "100%", textAlign: "center" }}>
                                <div>Silakan isi data dengan lengkap</div>
                            </ModalBody>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

export default Step2