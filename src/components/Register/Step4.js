import React from 'react';
import { Row, Col, FormGroup, Input, Label, Form, FormText, Container, Modal, ModalBody } from 'reactstrap';

class Step4 extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeQ1 = this.onChangeQ1.bind(this);
        this.onChangeQ2 = this.onChangeQ2.bind(this);
        this.onChangeQ3 = this.onChangeQ3.bind(this);
        this.onChangeQ4 = this.onChangeQ4.bind(this);
        this.onChangeQ5 = this.onChangeQ5.bind(this);
        this.toggle = this.toggle.bind(this)
        this.state = {
            q1: "",
            q2: "",
            q3: "",
            q4: "",
            q5: ""
        }
    }

    onChangeQ1(e) {
        this.setState({
            q1: e.target.value,
        })
        localStorage.setItem('BR2019_q1', e.target.value);
    }

    onChangeQ2(e) {
        this.setState({
            q2: e.target.value,
        })
        localStorage.setItem('BR2019_q2', e.target.value);
    }

    onChangeQ3(e) {
        this.setState({
            q3: e.target.value,
        })
        localStorage.setItem('BR2019_q3', e.target.value);
    }

    onChangeQ4(e) {
        this.setState({
            q4: e.target.value,
        })
        localStorage.setItem('BR2019_q4', e.target.value);
    }

    onChangeQ5(e) {
        this.setState({
            q5: e.target.value,
        })
        localStorage.setItem('BR2019_q5', e.target.value);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        })
    }

    render() {
        if (this.props.currentStep !== 4) {
            return null;
        }
        return (
            <div
                // className="register-border" 
                style={{
                    height: "100vh",
                    width: "100vw",
                    padding: "80px 15px 45px 15px"
                }}>
                <div style={{ backgroundColor: "white", maxWidth: "600px", height: "100%", borderRadius: "25px", margin: "auto" }}>
                    <div style={{
                        textAlign: "center",
                        fontSize: "1.3rem",
                        fontWeight: "500",
                        color: "white",
                        paddingTop: "15px",
                        paddingBottom: "5px",
                        backgroundColor: "#005498",
                        borderTopLeftRadius: "21px",
                        borderTopRightRadius: "21px"
                    }}>
                        Pendaftaran Peserta
                </div>
                    <div style={{ padding: "10px", fontSize: "0.7rem", fontWeight: "600", color: "#005498" }}>
                        <Form>
                            <FormGroup>
                                <Label>1.SUDAH PERNAH IKUT LOMBA LARI KATEGORI 5K/10K/21K ?</Label>
                                <Row>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q1" value="Ya" onChange={this.onChangeQ1} />{' '}
                                                Ya
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q1" value="Tidak" onChange={this.onChangeQ1} />{' '}
                                                Tidak
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Label for="examplePassword">2.APAKAH ANDA RUTIN BEROLAHRAGA ?</Label>
                                <Row>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q2" value="Ya" onChange={this.onChangeQ2} />{' '}
                                                Ya
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q2" value="Tidak" onChange={this.onChangeQ2} />{' '}
                                                Tidak
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Label for="examplePassword">3.APA GOLONGAN DARAH ANDA ?</Label>
                                <Row>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q3" value="A" onChange={this.onChangeQ3} />{' '}
                                                A
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q3" value="B" onChange={this.onChangeQ3} />{' '}
                                                B
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q3" value="AB" onChange={this.onChangeQ3} />{' '}
                                                AB
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q3" value="O" onChange={this.onChangeQ3} />{' '}
                                                O
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q3" value="Tidak Tahu" onChange={this.onChangeQ3} />{' '}
                                                Tidak Tahu
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Label for="examplePassword">4.APA ANDA MEMILIKI PENYAKIT ?</Label>
                                <Row>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q4" value="Ya" onChange={this.onChangeQ4} />{' '}
                                                Ya
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q4" value="Tidak" onChange={this.onChangeQ4} />{' '}
                                                Tidak
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </FormGroup>
                            <FormGroup>
                                <Label for="examplePassword">5.APA ANDA MEMILIKI ALERGI ?</Label>
                                <Row>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q5" value="Ya" onChange={this.onChangeQ5} />{' '}
                                                Ya
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                    <Col>
                                        <FormGroup check>
                                            <Label check>
                                                <Input type="radio" name="q5" value="Tidak" onChange={this.onChangeQ5} />{' '}
                                                Tidak
                                            </Label>
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </FormGroup>
                        </Form>
                        <div style={{ color: "red", fontWeight: "400" }}>
                            *) wajib diisi
                        </div>
                        {this.state.q1 != "" && this.state.q2 != "" && this.state.q3 != "" && this.state.q4 != "" && this.state.q5 != "" ? null :
                            <div className="btn" onClick={this.toggle}
                                style={{
                                    position: "fixed",
                                    top: "20px",
                                    right: "20px",
                                    borderRadius: "25em",
                                    zIndex: "1",
                                    color: "transparent",
                                    backgroundColor: "transparent",
                                    borderColor: "transparent"
                                }}>
                                Next <i className="nc-icon nc-minimal-right" style={{ top: "0" }} />
                            </div>}
                        <Modal isOpen={this.state.modal} toggle={this.toggle} >
                            <ModalBody style={{ width: "100%", textAlign: "center" }}>
                                <div>Silakan isi data dengan lengkap</div>
                            </ModalBody>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

export default Step4