import React from 'react';
import { Route } from 'react-router-dom'
import { Row, Col, FormGroup, Input, Label, Form, FormText, Container, Modal, ModalBody } from 'reactstrap';
// import Reaptcha from 'reaptcha';

class Step5 extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeKerabat = this.onChangeKerabat.bind(this);
        this.onChangeNoKerabat = this.onChangeNoKerabat.bind(this);
        this.onChangeHubungan = this.onChangeHubungan.bind(this);
        this.register = this.register.bind(this);
        this.toggle1 = this.toggle1.bind(this)
        this.state = {
            kerabat: "",
            nokerabat: "",
            hubungan: "",
            // verified: false
        }
    }

    onChangeKerabat(e) {
        this.setState({
            kerabat: e.target.value,
        })
        localStorage.setItem('BR2019_kerabat', e.target.value);
    }

    onChangeNoKerabat(e) {
        this.setState({
            nokerabat: e.target.value.replace(/\D/, ''),
        })
        localStorage.setItem('BR2019_nokerabat', e.target.value);
    }

    onChangeHubungan(e) {
        this.setState({
            hubungan: e.target.value,
        })
        localStorage.setItem('BR2019_hubungan', e.target.value);
    }

    toggle1() {
        this.setState({
            modal1: !this.state.modal1
        })
    }

    // onVerify = recaptchaResponse => {
    //     this.setState({
    //         verified: true
    //     });
    // };

    register() {
        // let kode_voucher = localStorage.getItem("BR2019_voucher")
        // let kategori = localStorage.getItem("BR2019_kategori")
        // let nik = localStorage.getItem("BR2019_nik")
        // let nama_lengkap = localStorage.getItem("BR2019_nama")
        // let gender = localStorage.getItem("BR2019_kelamin")
        // let tgl_lahir = localStorage.getItem("BR2019_lahir")
        // let alamat = localStorage.getItem("BR2019_alamat")
        // let kota = localStorage.getItem("BR2019_kota")
        // let kode_pos = localStorage.getItem("BR2019_pos")
        // let telp = localStorage.getItem("BR2019_telp")
        // let email = localStorage.getItem("BR2019_email")
        // let gol_darah = localStorage.getItem("BR2019_q1")
        // let pernah_ikut_lomba_lari = localStorage.getItem("BR2019_q2")
        // let rutin_olahraga = localStorage.getItem("BR2019_q3")
        // let penyakit = localStorage.getItem("BR2019_q4")
        // let alergi = localStorage.getItem("BR2019_q5")
        // let nama_kontak_darurat = localStorage.getItem("BR2019_kerabat")
        // let no_kontak_darurat = localStorage.getItem("BR2019_nokerabat")
        // let hubungan = localStorage.getItem("BR2019_hubungan")

        // fetch("https://apiserver.brirun.grit.id/User/Register", {
        //     method: 'POST',
        //     headers: { 'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8' },
        //     body: JSON.stringify({
        //         kode_voucher: kode_voucher,
        //         kategori: kategori,
        //         nik: nik,
        //         nama_lengkap: nama_lengkap,
        //         gender: gender,
        //         tgl_lahir: tgl_lahir,
        //         alamat: alamat,
        //         kota: kota,
        //         kode_pos: kode_pos,
        //         telp: telp,
        //         email: email,
        //         gol_darah: gol_darah,
        //         pernah_ikut_lomba_lari: pernah_ikut_lomba_lari,
        //         rutin_olahraga: rutin_olahraga,
        //         penyakit: penyakit,
        //         alergi: alergi,
        //         nama_kontak_darurat: nama_kontak_darurat,
        //         no_kontak_darurat: no_kontak_darurat,
        //         hubungan: hubungan
        //     })
        // })
        //     .then(response => {
        //         if (response.statusText == "OK") {
        //             localStorage.removeItem('BR2019_kategori', "")
        //             localStorage.removeItem('BR2019_voucher', "")
        //             localStorage.removeItem('BR2019_nik', "")
        //             localStorage.removeItem('BR2019_nama', "")
        //             localStorage.removeItem('BR2019_kelamin', "")
        //             localStorage.removeItem('BR2019_lahir', "")
        //             localStorage.removeItem('BR2019_alamat', "")
        //             localStorage.removeItem('BR2019_kota', "")
        //             localStorage.removeItem('BR2019_pos', "")
        //             localStorage.removeItem('BR2019_telp', "")
        //             localStorage.removeItem('BR2019_q1', "")
        //             localStorage.removeItem('BR2019_q2', "")
        //             localStorage.removeItem('BR2019_q3', "")
        //             localStorage.removeItem('BR2019_q4', "")
        //             localStorage.removeItem('BR2019_q5', "")
        //             localStorage.removeItem('BR2019_nokerabat', "")
        //             localStorage.removeItem('BR2019_hubungan', "")
        //             localStorage.removeItem('BR2019_kerabat', "")

        this.setState({ modal: true })
        //     }
        // })
        // .catch(error => {
        //     console.log(error)
        //     if (error != null) {
        //         this.setState({ modal1: true })
        //     }
        // })
    }

    render() {
        if (this.props.currentStep !== 5) {
            return null;
        }
        return (
            <div
                // className="register-border" 
                style={{
                    height: "100vh",
                    width: "100vw",
                    padding: "80px 15px 45px 15px"
                }}>
                <div style={{ backgroundColor: "white", maxWidth: "600px", height: "100%", borderRadius: "25px", margin: "auto" }}>
                    <div style={{
                        textAlign: "center",
                        fontSize: "1.3rem",
                        fontWeight: "500",
                        color: "white",
                        paddingTop: "15px",
                        paddingBottom: "5px",
                        backgroundColor: "#005498",
                        borderTopLeftRadius: "21px",
                        borderTopRightRadius: "21px"
                    }}>
                        Pendaftaran Peserta
                </div>
                    <div style={{ padding: "10px", fontSize: "0.7rem", fontWeight: "600", color: "#005498" }}>
                        <Form>
                            <FormGroup>
                                <Label>NAMA</Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    type="text"
                                    value={this.state.kerabat}
                                    placeholder="Masukkan Nama Anda"
                                    onChange={this.onChangeKerabat} />
                            </FormGroup>
                            {/* <FormGroup>
                                <Label>TELP</Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    type="text"
                                    maxLength="13"
                                    value={this.state.nokerabat}
                                    placeholder="No Telp Emergency"
                                    onChange={this.onChangeNoKerabat} />
                            </FormGroup>
                            <FormGroup>
                                <Label>HUBUNGAN</Label>
                                <Input style={{ fontSize: "0.8rem" }}
                                    type="text"
                                    value={this.state.hubungan}
                                    placeholder="Hubungan"
                                    onChange={this.onChangeHubungan} />
                            </FormGroup> */}
                        </Form>
                        <div style={{ color: "red", fontWeight: "400" }}>
                            *) wajib diisi
                        </div>
                        {/* <form style={{ padding: "20px" }}>
                            <Reaptcha sitekey="6LcgNcIUAAAAAFy6-0QrwBvcjQe8fa1AWMZfqyIG" onVerify={this.onVerify} />
                        </form> */}
                        <button style={{
                            marginTop: "10px",
                            color: "white",
                            backgroundColor: "#f68e1e",
                            borderStyle: "none",
                            borderRadius: "25px",
                            padding: "15px 15px 15px 15px",
                            width: "100px"
                        }} onClick={this.register}>
                            Daftar
                        </button>
                        <Modal isOpen={this.state.modal} >
                            <ModalBody style={{ width: "100%", textAlign: "center", fontSize: "2rem", fontWeight: "600" }}>
                                <div>Terima Kasih Sudah Mendaftar<br />Silakan periksa email untuk mendapatkan E-ticket</div>
                                {Button()}
                            </ModalBody>
                        </Modal>
                        <Modal isOpen={this.state.modal1} toggle={this.toggle1} >
                            <ModalBody style={{ width: "100%", textAlign: "center" }}>
                                <div>Periksa kembali data anda</div>
                            </ModalBody>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

const Button = () => (
    <Route render={({ history }) => (
        <button style={{
            marginTop: "10px",
            color: "white",
            backgroundColor: "#f68e1e",
            borderStyle: "none",
            borderRadius: "25px",
            padding: "15px 15px 15px 15px",
            width: "100px"
        }}
            type='button'
            onClick={() => { history.push('/index') }}
        >
            OK
      </button>
    )} />
)

export default Step5