import React, { useState } from "react";
import { Row, Col, FormGroup, Input, Label, Container, Form, Alert, Button, Modal, ModalBody } from 'reactstrap';

class Step1 extends React.Component {
    constructor(props) {
        super(props);
        this.onChangeVoucher = this.onChangeVoucher.bind(this);
        this.onChangeKategori = this.onChangeKategori.bind(this);
        this.toggle = this.toggle.bind(this)
        this.state = {
            voucher: "",
            kategori: "",
            modal: false
        }
    }

    onChangeKategori(e) {
        this.setState({
            kategori: e.target.value,
        })
        localStorage.setItem('BR2019_kategori', e.target.value);
    }

    onChangeVoucher(e) {
        this.setState({
            voucher: e.target.value,
        })
        localStorage.setItem('BR2019_voucher', e.target.value);
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        })
    }

    render() {

        if (this.props.currentStep !== 1) {
            return null;
        }

        return (
            <div
                // className="register-border" 
                style={{
                    height: "100vh",
                    width: "100vw",
                    padding: "80px 15px 45px 15px"
                }}>
                <div style={{ backgroundColor: "white", maxWidth: "600px", height: "100%", borderRadius: "25px", margin: "auto" }}>
                    <div style={{
                        textAlign: "center",
                        fontSize: "1.3rem",
                        fontWeight: "500",
                        color: "white",
                        paddingTop: "15px",
                        paddingBottom: "5px",
                        backgroundColor: "#005498",
                        borderTopLeftRadius: "21px",
                        borderTopRightRadius: "21px"
                    }}>
                        Pendaftaran Peserta
                    </div>
                    <div style={{ padding: "10px", fontSize: "0.7rem", fontWeight: "600", color: "#005498" }}>
                        <Form>
                            <FormGroup>
                                <Label>PILIH KATEGORI<span style={{ color: "red" }}>*</span></Label>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="radio" name="kategori" value="5" onChange={this.onChangeKategori} />{' '}
                                        5K CLOSED INDIVIDUAL
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="radio" name="kategori" value="10" onChange={this.onChangeKategori} />{' '}
                                        10K
                                    </Label>
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="radio" name="kategori" value="21" onChange={this.onChangeKategori} />{' '}
                                        21K
                                    </Label>
                                </FormGroup>
                            </FormGroup>
                            <FormGroup>
                                <Label for="examplePassword">KODE VOUCHER</Label>
                                <Input style={{ fontSize: "0.8rem" }} type="text" value={this.state.voucher} placeholder="Kode Voucher" onChange={this.onChangeVoucher} />
                            </FormGroup>
                        </Form>
                        <div style={{ color: "red", fontWeight: "400" }}>
                            *) wajib diisi
                        </div>
                        {this.state.kategori == "" ?
                            <div className="btn" onClick={this.toggle}
                                style={{
                                    position: "fixed",
                                    top: "20px",
                                    right: "20px",
                                    borderRadius: "25em",
                                    zIndex: "1",
                                    color: "transparent",
                                    backgroundColor: "transparent",
                                    borderColor: "transparent"
                                }}>
                                Next <i className="nc-icon nc-minimal-right" style={{ top: "0" }} />
                            </div> : null}
                        <Modal isOpen={this.state.modal} toggle={this.toggle} >
                            <ModalBody style={{ width: "100%", textAlign: "center" }}>
                                <div>Silakan pilih kategori lari</div>
                            </ModalBody>
                        </Modal>
                    </div>
                </div>
            </div>
        );
    }
}

export default Step1