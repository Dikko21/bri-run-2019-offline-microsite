import React, { Component } from 'react';
import auth0Client from '../../Auth';
import { withRouter } from 'react-router-dom';

// core components
import background from '../../assets/img/BG_plain_original.png';

class Registered extends Component {
    constructor(props) {
      super(props)
  
      this.signout = this.signout.bind(this)
    }

    signout() {
        auth0Client.signOut();
        this.props.history.push('/')
    }
    render() {
        return (
            <div
                style={{
                    backgroundImage: `url(${background})`,
                    backgroundPosition: "left",
                    backgroundSize: "cover",
                    minHeight: "0",
                    height: "100vh",
                    paddingTop: "35vh",
                    textAlign: "center"
                }}>
                <div
                    style={{
                        textAlign: "center",
                        backgroundColor: "white",
                        fontSize: "2rem",
                        fontWeight: "600",
                    }}>
                    Anda berada dalam waiting list peserta<br />
                    Silakan tunggu pengumuman
                </div>
                <button style={{
                    marginTop: "10px",
                    color: "white",
                    backgroundColor: "#f68e1e",
                    borderStyle: "none",
                    borderRadius: "25px",
                    padding: "15px 15px 15px 15px",
                    width: "100px"
                }} onClick={this.signout}>
                    Sign Out
                </button>
            </div>
        )
    }
}

export default withRouter(Registered)