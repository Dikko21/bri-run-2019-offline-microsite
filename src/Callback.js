import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import auth0Client from './Auth';
import background from './assets/img/BG_plain_original.png';

class Callback extends Component {
  async componentDidMount() {
    await auth0Client.handleAuthentication();
    this.props.history.replace('/register');
  }

  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${background})`,
          backgroundPosition: "left",
          backgroundSize: "cover",
          minHeight: "100vh",
          height: "100%",
          display: "flex",
          alignItems: "center",
          textAlign: "center"
        }}>
        <div className="lds-spinner">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
        {/* <p>Loading profile...</p> */}
      </div>
    );
  }
}

export default withRouter(Callback);