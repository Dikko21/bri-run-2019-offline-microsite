
import React, { useState, useEffect } from 'react';
// reactstrap components
import { Container, Row, Col } from "reactstrap";
// core components

const Iframe = ({ id }) => {
    return (
        <div id={id} >
            <Container style={{marginBottom:"4rem"}}>
                    <h2 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500",
                            marginBottom: "1rem"
                        }} >Lokasi</h2>
                    <div className="mapouter" style={{
                        position: "relative",
                        textAlign: "right",
                        Width: "800px",
                        Height: "400px",
                        margin: "auto",
                        border: "3px solid",
                        paddingTop: "5px",
                        paddingLeft: "5px",
                        paddingRight: "5px",
                        
                    }}>
                        <div className="gmap_canvas" style={{
                            overflow: "hidden",
                            background: "none!important",
                            Width: "800px",
                            Height: "400px",
                        }}>
                            <iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=lapangan%20kodam%20brawijaya%20surabaya&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
            </Container>
        </div>
    );
}
export default Iframe;
