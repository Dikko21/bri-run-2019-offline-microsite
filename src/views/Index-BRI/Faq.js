import React, { useState, useEffect } from 'react';
// reactstrap components
import { Container, Row, Col } from "reactstrap";
// core components

const Faq = ({ id }) => {
    return (
        <div id={id} className="">
            <Container>
                <Row>
                    <Col className="ml-auto mr-auto" md="12" >
                        <h2 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500",
                            marginBottom: "1rem"
                        }} >FAQ</h2>
                        <div>
                            <div>
                                <Row><Col className="terms-nama">1.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Apakah semua pendaftar otomatis menjadi peserta ?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Tidak semua pendaftar bisa menjadi peserta karena keterbatasan kuota, semua pendaftar akan masuk ke waiting list peserta. Yang dinyatakan sebagai peserta adalah yang mendapatkan kiriman email notifikasi dan e-tiket.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">2.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Apakah saya dapat mengalihkan kepesertaan saya?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Peserta tidak dapat mengalihkan kepesertaan mereka.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">3.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Berapakah saya harus membayar untuk kepesertaan saya?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Acara ini diadakan gratis (tidak berbayar) dalam rangka menyambut HUT BRI ke 124, sebagai bentuk rasa terima kasih Bank BRI kepada seluruh masyarakat Indonesia yang telah bersama-sama membesarkan Bank BRI selama lebih dari 1 abad.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">4.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Kapan pendaftaran ditutup?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Pendaftaran akan dibuka dan ditutup setiap harinya mulai tanggal 20 ~25 November 2019 jam 11:00 ~ 15:00 WIB</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">5.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Mengapa saya belum menerima e-ticket?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Ada 2 kemungkinan mengapa Anda belum menerima e-ticket:</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama"></Col><Col className="terms-isi">(a) Anda tidak terpilih sebagai peserta.</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama"></Col><Col className="terms-isi">(b) e-mail e-tiket masuk ke dalam spam folder (mohon periksa spam folder Anda), e-tiket juga bisa dilihat melalui aplikasi BRILian RUN 2019.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">6.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Adakah pendaftaran melalui walk-in/on-the-spot saat event berlangsung?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Tidak ada. Semua peserta disarankan untuk melakukan pendaftaran secara online melalui aplikasi BRILian RUN yang dapat diunduh di Google Play dan Apple Store.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">7.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Berapa ukuran dimensi shirt yang akan digunakan?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Shirt atau kaos yang akan digunakan memakai dimensi all size.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">8.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Kapan dan dimanakah Race Pack Collection diadakan ?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Kami masih melakukan finalisasi tempat Race Pack Collection diadakan. Informasi akan segera disampaikan setelah tersedia. Mohon ikuti social media kami yaitu Facebook, Twitter dan Instagram kami untuk update.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">9.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Di mana titik Start dan Finish?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Titik start dan finish adalah di Lapangan Makodam V Brawijaya.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">10.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Pukul berapa start lomba ?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Waktu start bisa dicek di informasi event.</Col></Row>
                                <br />
                                <Row><Col className="terms-nama">11.</Col><Col className="terms-nama">Q</Col><Col className="terms-isi">Berapa hadiah buat para pemenang ?</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-nama">A</Col><Col className="terms-isi">Organizer sedang menyusun perincian hadiah buat pemenang. Informasi akan segera disampaikan setelah tersedia. Mohon ikuti social media kami di facebook, twitter dan instagram kami untuk update terbaru.</Col></Row>
                            </div>
                        </div>
                        <br /><hr />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
export default Faq;
