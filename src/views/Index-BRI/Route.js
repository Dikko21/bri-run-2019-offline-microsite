
import React, { useState, useEffect } from 'react';
// reactstrap components
import { Container, Row, Col, Modal, ModalBody } from "reactstrap";
import ImageComponent from "./pop";
// core components
import route1 from "../../assets/img/BRI_RUN_2019-RUTE_FA-02.png";
import route2 from "../../assets/img/BRI_RUN_2019-RUTE_FA-01.png";
import route3 from "../../assets/img/BRI_RUN_2019-RUTE_FA-03.png";
import route1big from "../../assets/img/BRI_RUN_2019-RUTE_FA-02.png";
import route2big from "../../assets/img/BRI_RUN_2019-RUTE_FA-01.png";
import route3big from "../../assets/img/BRI_RUN_2019-RUTE_FA-03.png";
import desktopImage from '../../assets/img/background-abstract.PNG';

const Faq = ({ id }) => {
    const [modal1, setModal1] = useState(false);
    const [modal2, setModal2] = useState(false);
    const [modal3, setModal3] = useState(false);

    const toggle1 = () => setModal1(!modal1);
    const toggle2 = () => setModal2(!modal2);
    const toggle3 = () => setModal3(!modal3);

    return (
        <div id={id} className="" style={{ backgroundImage: `url(${desktopImage})` }}>
            <div style={{ paddingLeft: "10%", paddingRight: "10%" }}>
                <Row>
                    <Col className="ml-auto mr-auto" lg="12">
                        <h2 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500",
                            marginBottom: "1rem"
                        }} >Rute</h2>
                    </Col>
                    <Col className="ml-auto mr-auto" md="12" lg="6" style={{marginTop:"20px"}}>
                        <div style={{ border: "3px solid", margin: "1%", backgroundColor:"white" }}>
                            <img style={{ width: "100%" }} src={route1} alt="Logo" onClick={toggle1}/>
                            <h5 style={{ textAlign: "center", fontWeight: "bold" }}>Rute 21K</h5>
                        </div>
                    </Col>
                    <Col>
                    <Row>
                    <Col className="ml-auto mr-auto" md="12" lg="12" style={{marginTop:"20px"}}>
                        <div style={{ border: "3px solid", margin: "1%", backgroundColor:"white" }}>
                            <img style={{ width: "100%" }} src={route2} alt="Logo" onClick={toggle2}/>
                            <h5 style={{ textAlign: "center", fontWeight: "bold", marginBottom: "0" }}>Rute 10K</h5>
                        </div>
                    </Col>
                    <Col className="ml-auto mr-auto" md="12" lg="12" style={{}}>
                        <div style={{ border: "3px solid", margin: "1%", backgroundColor:"white" }}>
                            <img style={{ width: "100%" }} src={route3} alt="Logo" onClick={toggle3}/>
                            <h5 style={{ textAlign: "center", fontWeight: "bold", marginBottom: "0", marginTop: "6px" }}>Rute 5K</h5>
                        </div>
                    </Col>
                    </Row>
                    </Col>
                </Row>
            </div>
            <br /><br />
            <Modal isOpen={modal1} toggle={toggle1} >
                <ModalBody style={{width: "100%"}}>
                    <img style={{ width: "100%" }} src={route1big} alt="Logo" />
                </ModalBody>
            </Modal>
            <Modal isOpen={modal2} toggle={toggle2} >
                <ModalBody style={{width: "100%"}}>
                    <img style={{ width: "100%" }} src={route2big} alt="Logo" />
                </ModalBody>
            </Modal>
            <Modal isOpen={modal3} toggle={toggle3} >
                <ModalBody style={{width: "100%"}}>
                    <img style={{ width: "100%" }} src={route3big} alt="Logo" />
                </ModalBody>
            </Modal>
        </div>
    );
}
export default Faq;
