
import React, { useState, useEffect } from 'react';
// reactstrap components
import { Container, Row, Col } from "reactstrap";
// core components
import { Link, animateScroll as scroll } from "react-scroll";

const Terms = ({ id }) => {
    return (
        <div id={id} className="">
            <Container>
                <Row>
                    <Col className="ml-auto mr-auto" md="12" >
                        <h2 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500",
                            marginBottom: "1rem"
                        }} >Syarat & Ketentuan</h2>
                        <h3 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500"
                        }} >CARA PENDAFTARAN</h3>
                        <br />
                        <div>
                            <div>
                                <Row><Col className="terms-nama">1</Col><Col className="terms-isi">BRILian RUN 2019 adalah lomba lari dengan kategori CLOSED INDIVIDUAL yang mana hanya boleh diikuti oleh Warga Negara Indonesia.</Col></Row>
                                <Row><Col className="terms-nama">2</Col><Col className="terms-isi">Pendaftar yang berhak untuk menjadi peserta adalah Warga Negara Indonesia yang wajib memiliki data diri berupa KTP atau passport yang masih berlaku.</Col></Row>
                                <Row><Col className="terms-nama">3</Col><Col className="terms-isi">Peserta wajib mengisi format pendaftaran yang disediakan dengan data yang akurat dan benar sesuai dengan identitas resmi serta tidak diperbolehkan menggunakan nama inisial.</Col></Row>
                                <Row><Col className="terms-nama">4</Col><Col className="terms-isi">Pendaftaran dibuka mulai tanggal 20 ~ 25 November 2019.</Col></Row>
                                <Row><Col className="terms-nama">5</Col><Col className="terms-isi">Jumlah peserta sebanyak 10.000 peserta.</Col></Row>
                                <Row><Col className="terms-nama">6</Col><Col className="terms-isi">Sistem pendaftaran secara ONLINE via aplikasi BRILian RUN (android dan iOS).</Col></Row>
                                <Row><Col className="terms-nama">7</Col><Col className="terms-isi">Waktu pendaftaran :</Col></Row>
                                <Row><Col className="terms-nama"></Col><Col className="terms-isi">Pendaftaran hanya dibuka dari jam 11.00 ~ 15.00 WIB (selama 5 hari).</Col></Row>
                                <Row><Col className="terms-nama">8</Col><Col className="terms-isi">Setiap pendaftar yang sudah melakukan registrasi akan masuk kedalam waiting list peserta dan akan mendapatkan email notifikasi.</Col></Row>
                                <Row><Col className="terms-nama">9</Col><Col className="terms-isi">Pendaftar harus mengisi data secara benar dan lengkap sesuai identitas, pendaftar yang melakukan pendaftaran lebih dari satu kali akan dinyatakan gugur.</Col></Row>
                                <Row><Col className="terms-nama">10</Col><Col className="terms-isi">Pihak penyelenggara akan melakukan validasi data pendaftar.</Col></Row>
                                <Row><Col className="terms-nama">11</Col><Col className="terms-isi">Bagi pendaftar yang terpilih menjadi peserta akan diumumkan melalui microsite www.brilianrun.com setiap hari mulai pukul 09:00 WIB dan akan mendapatkan e-tiket untuk ditukarkan saat pengambilan
                                    <Link activeClass="active" to="rpc" spy={true} smooth={true}
                                        offset={-100}
                                        duration={500}
                                    >
                                        <a href="" style={{color:"blue"}}> Race Pack.</a>
                                    </Link></Col></Row>
                                <Row><Col className="terms-nama">12</Col><Col className="terms-isi">Bagi yang berhasil menjadi peserta BRIlian Run 2019 akan dikirimkan email mengenai tata cara untuk mendapatkan e-tiket.</Col></Row>
                                <Row><Col className="terms-nama">13</Col><Col className="terms-isi">Keputusan pihak penyelenggara mutlak dan tidak dapat di ganggu gugat.</Col></Row>
                                <Row><div className="terms-note"> <small>Notes :- Pihak penyelenggara akan melakukan validasi data pendaftar secara harian. Per hari akan diambil 2.000 pendaftar tercepat dengan data valid.</small></div></Row>
                            </div>
                        </div>
                    </Col>
                    <Col className="ml-auto mr-auto" md="12" >
                        <h3 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500"
                        }} >KONDISI PESERTA</h3>
                        <br />
                        <div>
                            <div>
                                <Row><Col className="terms-nama">1</Col><Col className="terms-isi">Peserta wajib menggunakan nomor dada (BIBs) yang dipasang di bagian depan tubuh sehingga nomor dada dapat terbaca dengan jelas.</Col></Row>
                                <Row><Col className="terms-nama">2</Col><Col className="terms-isi">Peserta wajib menggunakan sepatu dan celana olahraga, serta baju lari yang telah disediakan saat pengambilan race pack.</Col></Row>
                                <Row><Col className="terms-nama">3</Col><Col className="terms-isi">Peserta dilarang bertelanjang dada selama mengikuti event.</Col></Row>
                                <Row><Col className="terms-nama">4</Col><Col className="terms-isi">Peserta diharuskan berlari pada rute race yang disiapkan oleh pihak penyelenggara.</Col></Row>
                                <Row><Col className="terms-nama">5</Col><Col className="terms-isi">Apabila terdapat peserta yang sakit selama event berlangsung, maka peserta mengijinkan staf medis untuk memberikan penanganan yang diperlukan untuk perawatan pasien.</Col></Row>
                                <Row><Col className="terms-nama">6</Col><Col className="terms-isi">Penyelenggara berhak bertindak dan mengintervensi jika ada hal-hal yang dapat membahayakan keselamatan semua pihak.</Col></Row>
                                <Row><Col className="terms-nama">7</Col><Col className="terms-isi">Penyelenggara tidak bertanggung jawab atas kejadian seperti cedera, cacat bahkan kematian yang terjadi untuk setiap peserta sebelum, selama atau sesudah race berlangsung.</Col></Row>
                                <Row><Col className="terms-nama">8</Col><Col className="terms-isi">Penyelenggara berhak untuk menggunakan foto, rekaman film, atau catatan lain dari kegiatan untuk tujuan yang sah, termasuk iklan komersial.</Col></Row>
                                <Row><Col className="terms-nama">9</Col><Col className="terms-isi">Penyelenggara berhak merubah aturan kapan saja yang dianggap perlu dan sesuai dengan keadaan baik sebelum dan saat event berlangsung tanpa pemberitahuan terlebih dahulu beserta alasannya.</Col></Row>
                                <Row><Col className="terms-nama">10</Col><Col className="terms-isi">Peserta wajib mempelajari rute race serta memahami semua ketentuan acara.</Col></Row>
                                <Row><Col className="terms-nama">11</Col><Col className="terms-isi">Peserta tidak boleh memberikan hak pendaftaran, nomor dada untuk digunakan pihak lain mengikuti event.</Col></Row>
                                <Row><Col className="terms-nama">12</Col><Col className="terms-isi">Pemenang harus sesuai dengan nama, BIB / nomor dada yang terdaftar, dengan bukti pendaftaran dan KTP / Paspor yang berlaku.</Col></Row>
                                <Row><Col className="terms-nama">13</Col><Col className="terms-isi">Pemenang doorprize yang tidak dapat membuktikan identitas diri sesuai bukti pendaftaran, BIB / nomor dada, KTP, maka penyelenggara dapat memutuskan untuk menggugurkan / mendiskualifikasi pemenang tersebut.</Col></Row>
                                <Row><Col className="terms-nama">14</Col><Col className="terms-isi">Untuk kelancaran lomba, peserta dianjurkan melakukan konsultasi kesehatan ke dokter/medical checkup sebelum acara.</Col></Row>
                            </div>
                        </div>
                        <div></div>
                        <br /><hr />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
export default Terms;
