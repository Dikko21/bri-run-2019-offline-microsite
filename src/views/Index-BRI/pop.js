import React from "react";

import route1 from "../../assets/img/Route-1.png";

export default class ImageComponent extends React.Component {
    state = { isOpen: false };

    handleShowDialog = () => {
        this.setState({ isOpen: !this.state.isOpen });
        console.log("cliked");
    };

    render() {
        return (
            <div style={{ border: "3px solid", margin: "1%" }}>
                <img
                    className="small"
                    src={route1}
                    style={{ width: "100%" }}
                    onClick={this.handleShowDialog}
                    alt="no image"
                />
                <h5 style={{ textAlign: "center", fontWeight: "bold" }}>Rute 5K</h5>
                {this.state.isOpen && (
                    <dialog
                        style={{ position: "absolute" }}
                        open
                        onClick={this.handleShowDialog}
                    >
                        <img
                            src={route1}
                            style={{position: "static", width:"90vw", zIndex:"9999999"}}
                            onClick={this.handleShowDialog}
                            alt="no image"
                        />
                    </dialog>
                )}
            </div>
        );
    }
}
