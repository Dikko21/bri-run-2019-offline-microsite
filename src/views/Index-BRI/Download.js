import React, { useState, useEffect } from 'react';
import {
    Container,
    Row,
    Col,
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter
} from "reactstrap";
import desktopImage from '../../assets/img/1366x768-dark.png';
import mobileImage from '../../assets/img/1024x768-dark.png';
import mobileImage2 from '../../assets/img/768x1024-dark.png';
import mobileImage3 from '../../assets/img/512x1024-dark.png';
import gambar from "assets/img/appdownload.png";

const Download = ({ id }) => {
    const imageUrl = useWindowWidth() <= 1200 ? mobileImage : desktopImage;
    const imageUrl2 = useWindowWidth() <= 800 ? mobileImage2 : imageUrl;
    const imageUrl3 = useWindowWidth() <= 600 ? mobileImage3 : imageUrl2;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return (
        <div className="page-header " id={id} style={{ backgroundImage: `url(${imageUrl3})` }}>
            <Container>
                <Row className="ml-auto mr-auto">
                    <Col className="ml-auto mr-auto d-flex align-items-center" md="6">
                        <div>
                            {/* <h2 style={{ fontWeight: "bold", color: "white" }}>
                                KAMI MEMBUAT<br />REVOLUSI DI APLIKASI
                            </h2> */}
                            <p style={{ color: "white" }}>
                                Segera download aplikasi BRILian RUN dan daftarkan diri anda.
                            </p>
                            <Row>
                                <Col className="ml-auto mr-auto" md="6">
                                    <a href="#!" className="btn btn-apple" style={{ width: "160px" }} onClick={toggle}>
                                        <span className="float-right g-font-size-13 color-download">
                                            Download App
                                            <span className="d-block g-font-size-10 g-font-weight-400 g-opacity-0_6">
                                                From App Store
                                            </span>
                                        </span>
                                        <i className="fa fa-apple fa-2x apple-size" />
                                    </a>
                                </Col>
                                <Col className="ml-auto mr-auto" md="6">
                                    <a href="#!" className="btn btn-android" style={{ width: "160px" }} onClick={toggle}>
                                        <span className="float-right g-font-size-13 color-download">
                                            Download App
                                            <span className="d-block g-font-size-10 g-font-weight-400 g-opacity-0_6">
                                                From Play Market
                                            </span>
                                        </span>
                                        <i className="fa fa-android fa-2x android-size" />
                                    </a>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    <Col className="ml-auto mr-auto" md="6">
                        <img className="img-fluid" src={gambar} alt="Image description" data-animation="slideInUp" />
                    </Col>
                </Row>
            </Container>
            <Modal isOpen={modal} toggle={toggle} >
                <ModalBody>
                    aplikasi baru bisa diunduh tanggal 20 November 2019
                </ModalBody>
            </Modal>
        </div>
    );
};

const useWindowWidth = () => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    const handleWindowResize = () => {
        setWindowWidth(window.innerWidth);
    };

    useEffect(() => {
        window.addEventListener('resize', handleWindowResize);
        return () => window.removeEventListener('resize', handleWindowResize);
    }, []);

    return windowWidth;
};

export default Download;