import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from "reactstrap";
import ig from "../../assets/img/Logo_IG.png";
import fb from "../../assets/img/Logo_FB.png";
import twitter from "../../assets/img/Logo_TW.png";
import youtube from "../../assets/img/Logo_YT.png";
// import desktopImage from '../../assets/img/background-abstract.PNG';

const Media = ({ id }) => {
    return (
        <div id={id} className="footer-bri" style={{ marginBottom: "4rem" }}>
            {/* <div id={id} className="footer-bri" style={{backgroundImage: `url(${desktopImage})`}}> */}
            <Container>
                <Row className="ml-auto mr-auto d-flex align-items-center">
                    <Col className="ml-auto mr-auto" md="12">
                        <h2 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500",
                            marginBottom: "1rem"
                        }} >Hubungi Kami</h2>
                    </Col>
                    <Col md="12" style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "800",
                            marginBottom: "1rem",
                            fontSize: "1.2rem"
                        }}>
                        email : info@brilianrun.com
                    </Col>
                    <Col className="ml-auto mr-auto" sm="6" lg="3">
                        <a href={"https://www.instagram.com/bankbri_id/"}>
                            <img className="icon-share" style={{ width: "100%" }} src={ig}></img>
                        </a>
                    </Col>
                    <Col className="ml-auto mr-auto" sm="6" lg="3">
                        <a href={"https://www.facebook.com/BRIofficialpage"}>
                        <img className="icon-share" style={{ width: "100%" }} src={fb}></img>
                        </a>
                    </Col>
                    <Col className="ml-auto mr-auto" sm="6" lg="3">
                        <a href={"https://twitter.com/kontakbri"}>
                        <img className="icon-share" style={{ width: "100%" }} src={twitter}></img>
                        </a>
                    </Col>
                    <Col className="ml-auto mr-auto" sm="6" lg="3">
                        <a href={"https://youtube.com/channel/UCRHFE_ooDrkEiRRJbog3EjA"}>
                        <img className="icon-share" style={{ width: "100%" }} src={youtube}></img>
                        </a>
                    </Col>
                </Row>
                <hr/>
            </Container>
        </div>
    );
}

export default Media;
