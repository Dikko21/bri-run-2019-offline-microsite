
import React, { useState, useEffect } from 'react';
// reactstrap components
import { Container, Row, Col } from "reactstrap";
// core components
import route1 from "../../assets/img/RPC1SQ.png";
import route2 from "../../assets/img/RPC2SQ.png";
import route3 from "../../assets/img/RPC3SQ.png";
import route4 from "../../assets/img/RPC4SQ.jpeg";
import route5 from "../../assets/img/RPC5-1.jpeg";
import route6 from "../../assets/img/RPC5-2.jpeg";
import route7 from "../../assets/img/RPC5-3.jpeg";
import desktopImage from '../../assets/img/background-abstract.PNG';

const Faq = ({ id }) => {
    return (
        <div id={id} className="" style={{ backgroundImage: `url(${desktopImage})` }}>
            <div style={{paddingLeft:"10%", paddingRight:"10%"}}>
            <Row>
                <Col className="ml-auto mr-auto" lg="12">
                    <h2 style={{
                        textAlign: "center",
                        color: "rgb(110, 110, 110)",
                        fontWeight: "500",
                        marginBottom: "1rem"
                    }} >Race Pack</h2>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="6" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px", backgroundColor:"white" }}>
                        <img style={{ width: "100%" }} src={route1} alt="Logo" />
                    </div>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="6" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px", backgroundColor:"white" }}>
                        <img style={{ width: "100%" }} src={route4} alt="Logo" />
                    </div>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="6" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px" , backgroundColor:"white"}}>
                        <img style={{ width: "100%" }} src={route2} alt="Logo" />
                    </div>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="6" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px", backgroundColor:"white" }}>
                        <img style={{ width: "100%" }} src={route3} alt="Logo" />
                    </div>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="4" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px", backgroundColor:"white" }}>
                        <img style={{ width: "100%" }} src={route5} alt="Logo" />
                    </div>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="4" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px", backgroundColor:"white" }}>
                        <img style={{ width: "100%" }} src={route6} alt="Logo" />
                    </div>
                </Col>
                <Col className="ml-auto mr-auto" md="12" lg="4" style={{marginTop:"20px"}}>
                    <div style={{ border: "3px solid", margin: "1%", padding: "10px", backgroundColor:"white" }}>
                        <img style={{ width: "100%" }} src={route7} alt="Logo" />
                    </div>
                </Col>
            </Row>
            </div>
            <br /><br />
        </div>
    );
}
export default Faq;
