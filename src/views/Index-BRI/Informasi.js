
import React, { useState, useEffect } from 'react';
// reactstrap components
import { Container, Row, Col, Button } from "reactstrap";
// core components
import { Link } from "react-scroll";

import desktopImage from '../../assets/img/back-informasi.png';
import schedule from '../../assets/img/Schedule.jpeg';

const Informasi = ({ id }) => {
    const imageUrl = useWindowWidth() <= 575 ? desktopImage : desktopImage;
    return (
        <div id={id} className="" style={{
            backgroundImage: `url(${imageUrl})`,
            backgroundSize: "cover",
            backgroundRepeat: "noRepeat"
        }}>
            <Container>
                <Row>
                    <Col className="ml-auto mr-auto" md="12" >
                        <h2 style={{
                            textAlign: "center",
                            color: "rgb(110, 110, 110)",
                            fontWeight: "500",
                            marginBottom: "1rem"
                        }} >INFORMASI EVENT</h2>
                        <div style={{
                            width: "100%",
                            color: "rgb(110, 110, 110)"
                        }}>
                            <div style={{
                                maxWidth: "45rem",
                                margin: "0 auto"
                            }}>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Nama Event</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" xs="6" style={{paddingLeft:"3px"}}>BRILian RUN 2019 Surabaya Series</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Kategori</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>21K (HM), 10K, 5K CLOSED INDIVIDUAL</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Hari / Tanggal</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>Minggu / 1 Desember 2019</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Waktu</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>05:00 WIB - selesai</Col></Row>
                                {/* <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Waktu Start</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}></Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Kategori 21K (HM)</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>05:00 WIB</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>FINISH COT 21K (HM)</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>09:00 WIB</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Kategori 10K</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>06:00 WIB</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>FINISH COT 10K</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>07:55 WIB</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Kategori 5K</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>06:30 WIB</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>FINISH COT 5K</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>07:27 WIB</Col></Row><br/>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>CUT OFF POINT 21K</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}></Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>KM 10</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>05:55 WIB</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>KM 15</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>07:52 WIB</Col></Row> */}
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}>Start / Finish</Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}>:</Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>Lap Makodam V Brawijaya Surabaya</Col></Row>
                                <Row><Col xs="2"></Col><Col xs="3" className="paragraf-bri" style={{maxWidth:"11rem", paddingRight:"0"}}></Col><Col xs="2" className="paragraf-bri" style={{maxWidth:"0.5rem", padding : "0"}}></Col><Col className="paragraf-bri" style={{paddingLeft:"3px"}}>Jl.Pulosari III No 87 Sawunggaling Surabaya</Col></Row>
                                <img style={{ width: "100%" }} src={schedule} alt="Logo"/>
                            </div>
                        </div>
                        <br />
                        <p className="paragraf-bri">
                            BRILian RUN merupakan kegiatan olah raga lari rutin tahunan yang diselenggarakan
                            oleh Bank BRI dalam rangka menyambut Hari Ulang Tahun Bank BRI ke 124 pada
                            tanggal 1 Desember 2019 mendatang. Kegiatan ini diperuntukkan bagi seluruh
                            lapisan masyarakat, terutama para pecinta olah raga lari.
                            </p><br />
                        <p className="paragraf-bri">
                            BRILian RUN dikemas dalam tiga kategori lari, yakni kategori fun run 5K, kompetisi
                            pada kategori Olympic Run 10K dan 21K acara ini terbuka untuk 10.000 peserta,
                            pendaftaran dilakukan tanpa dipungut biaya apapun, alias gratis.
                            </p><br />
                        <p className="paragraf-bri">
                            Setiap peserta akan diberikan <span style={{ fontStyle: "italic" }}>race pack</span> menarik berupa Running Tees, BIB, Spun Bond Bag dan Race Booklet. Pendaftaran dapat dilakukan melalui aplikasi BRILian RUN yang dapat diunduh di{' '}
                            <label style={{ margin: "0" }}>
                                <Link activeClass="active" to="download" spy={true} smooth={true}
                                    offset={-55} duration={500}
                                >
                                    <span className="btn-scroll-android" style={{ borderRadius: "5px" }}>
                                        <i className="fa fa-android android-size" />Google Play
                                    </span>
                                </Link>
                            </label>
                            {' '}maupun{' '}
                            <label style={{ margin: "0" }}>
                                <Link activeClass="active" to="download" spy={true} smooth={true}
                                    offset={-55} duration={500}
                                >
                                    <span className="btn-scroll-android" style={{ borderRadius: "5px" }}>
                                        <i className="fa fa-apple apple-size" />App Store
                                    </span>
                                </Link>
                            </label>
                            {' '}mulai tanggal 20 November 2019.
                            </p><br />
                        <p className="paragraf-bri">
                            Nikmati pengalaman baru berlari dengan berbagai menu digital lainnya pada gelaran
                            BRILian RUN ini. BRILian RUN juga akan menyajikan hiburan menarik berupa penampilan Andra and The Backbone bersama pengisi acara lainnya di Race Village area, yang akan dipandu oleh duet host kenamaan Valentino "Jebret" Simanjuntak dan Kamidia Radisti. Nikmati juga bazaar kuliner yang menghadirkan tenant-tenant
                            kuliner favorit di kota Surabaya. Jangan lupa, BRILian RUN juga akan menghadirkan
                             promo belanja menarik di area yang sama melalui pemanfaatan uang isi ulang BRI BRIZZI dan Link Aja.
                        </p><br />
                        <p className="paragraf-bri">
                            Rangkaian acara ini bekerja sama dengan Seger Waras 360 sebagai pengelola acara.
                        </p><br />
                        <p className="paragraf-bri">
                            Segera daftarkan diri Anda, dan rasakan kemeriahan acaranya!
                        </p><br /><br />
                    </Col>
                </Row>
            </Container>
        </div >
    );
}

const useWindowWidth = () => {
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    const handleWindowResize = () => {
        setWindowWidth(window.innerWidth);
    };

    useEffect(() => {
        window.addEventListener('resize', handleWindowResize);
        return () => window.removeEventListener('resize', handleWindowResize);
    }, []);

    return windowWidth;
};

export default Informasi;
