import auth0 from 'auth0-js';

class Auth {
    constructor() {
        this.auth0 = new auth0.WebAuth({
            // the following three lines MUST be updated
            domain: 'dev-y67inxhz.au.auth0.com',
            audience: 'https://dev-y67inxhz.au.auth0.com/userinfo',
            clientID: '5lbLKpFBwpPCaA41sNG0XKRZEValtpvS',
            redirectUri: 'http://localhost:3000/callback',
            responseType: 'id_token',
            scope: 'openid profile email'
        });

        this.getProfile = this.getProfile.bind(this);
        this.handleAuthentication = this.handleAuthentication.bind(this);
        this.isAuthenticated = this.isAuthenticated.bind(this);
        this.signIn = this.signIn.bind(this);
        this.signOut = this.signOut.bind(this);
    }

    getProfile() {
        return this.profile;
    }

    getIdToken() {
        return this.idToken;
    }

    isAuthenticated() {
        try {
            return new Date().getTime() < localStorage.getItem('expiresAt');
        }
        catch(e) {
            return false;
        }
    }

    signIn() {
        this.auth0.authorize();
    }

    handleAuthentication() {
        return new Promise((resolve, reject) => {
            this.auth0.parseHash((err, authResult) => {
                if (err) return reject(err);
                if (!authResult || !authResult.idToken) {
                    return reject(err);
                }
                localStorage.setItem('idToken', authResult.idToken);
                localStorage.setItem('profile', JSON.stringify(authResult.idTokenPayload));
                localStorage.setItem('expiresAt', authResult.idTokenPayload.exp * 1000 + new Date().getTime());
                resolve();
            });
        })
    }

    signOut() {
        // clear id token, profile, and expiration
        localStorage.removeItem('idToken');
        localStorage.removeItem('profile');
        localStorage.removeItem('expiresAt');
        localStorage.clear();
    }
}

const auth0Client = new Auth();

export default auth0Client;